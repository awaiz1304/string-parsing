import React,{useState,useEffect,useRef} from 'react'
import './App.css';
import { matchString } from './Utility/matchString';

function App() {
  const [dateTimeStr,setDateTime] = useState('')
  const [body,setBody] = useState('')
  const [mentions,setMentions] = useState('')
  const OnClickSubmit = () => {
    let inputStr = document.getElementById('inputStr').value
    let obj = matchString(inputStr)
    setDateTime('Date Time - '+obj.dateTime)
    setBody('Body - '+obj.body)
    setMentions('Mentions - '+obj.mentions)
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="row">
          <input id="inputStr"></input>
          <button onClick = {OnClickSubmit} className="submit-btn" type="submit">Submit</button>
        </div>
        <p>
          {dateTimeStr}<br/>
          {body}<br/>
          {mentions}<br/>
        </p>
        
      </header>
    </div>
  );
}

export default App;
