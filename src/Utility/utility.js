export const weekdays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
export const dayTime = {
    morning : '09:00:00',
    noon : '18:00:00',
    night : '22:00:00'
}
export const day = ['today','tomorrow']

export const refStrings = {
    type1: 'Friday at Noon Lunch with team @ATeam',
    type2: '2/27/2021 7 p.m. Happy Hour with @Elliot @Trav',
    type3: 'Wake Up Call at 8am',
    type4: 'Tomorrow 3:30 PM https://TextHotline.com - @Elliot, @Trav, and @David'
}

export const checkForDateStr = (dateStr) => {
    var re = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/
    return re.test(String(dateStr).toLowerCase());
}
export const getTime = (dateStr=null,offset=-6) => {
    let d
    if(dateStr != null){
        d = new Date(dateStr);
    }else{
        d = new Date()
    }
    var utc = d.getTime()
    utc = utc + (d.getTimezoneOffset() * 60000);
    var nd = new Date(utc + (3600000*offset));
    return nd;
}


