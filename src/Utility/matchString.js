import { 
    getTime,
    weekdays,
    refStrings,
    checkForDateStr,
    day, 
    dayTime
} from './utility';
const extractDate = (strSplitted,refStr) => {
    let firstWord = strSplitted[0].toLowerCase()
    let currentDate = getTime()
    let minutes = 0
    if(refStr == refStrings.type1){
        let weekDay = currentDate.getDay()
        let dayToFindIndex = weekdays.findIndex(x=>x.toLowerCase() == firstWord)
        currentDate.setDate(weekDay+dayToFindIndex)
        let time = strSplitted[2].toLowerCase()
        if(dayTime[time] != null){
            let timeSpt = dayTime[time].split(":")
            currentDate.setHours(timeSpt[0])
        }
        
    }else if(refStr == refStrings.type2 || refStr == refStrings.type4){
        let date 
        if(refStr == refStrings.type2){
            date = firstWord
        }else{
            let index = day.findIndex(x=>x.toLowerCase() == firstWord)
            date = (currentDate.getMonth()+1)+"/"+(currentDate.getDate()+index)+"/"+currentDate.getFullYear()
        }
        let timeSplt = strSplitted[1].split(":")
        let hour = parseInt(timeSplt[0])
        if(isNaN(hour)){
            timeSplt = dayTime[strSplitted[1]].split(":")
            currentDate = new Date(date)
            currentDate.setHours(timeSplt[0])
        }else{
            if(timeSplt[1] != null){
                minutes = parseInt(timeSplt[1])
            }
            let ampm = strSplitted[2]
            if(ampm == "p.m." || ampm == "PM"){
                hour += 12
            }
            date = date+" "+hour+":"+minutes+":00"
            date = getTime(date)
            currentDate = date
        }
    }else{
        let days = strSplitted.filter((e,i)=>{
            return day.indexOf(e) > -1
        },day)
        let index
        if(days.length > 0){
            index = day.findIndex(x=>x.toLowerCase() == days[0].toLowerCase())
        }
        let timeStr = strSplitted[strSplitted.length - 1]
        let time = parseInt(timeStr.charAt(0))
        if(!isNaN(time)){
            let ampm = timeStr.slice(1,3)
            if(ampm == "pm"){
                time += 12
            }
            if(time > 8){
                currentDate.setDate(currentDate.getDate()+1)
            }
        }else{
            time = dayTime[firstWord.toLowerCase()]
            if(time != null){
                currentDate.setHours(time.split(":")[0])
            }
            if(index > -1){
                currentDate.setDate(currentDate.getDate()+index)
            }
        }
    }
    currentDate.setMinutes(minutes)
    currentDate.setSeconds(0)
    currentDate.setMilliseconds(0)
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    var localISOTime = (new Date(currentDate.getTime() - tzoffset)).toISOString();
    return localISOTime
}
const extractBody = (strSplitted,refStr,lastCount) => {
    let body = ''
    let startIndex = 3
    if(refStr == refStrings.type3){
        lastCount = strSplitted.findIndex(x=>x.toLowerCase() == "at")
        startIndex = 0
    }
    for(var i=startIndex;i<lastCount;i++){
        body += strSplitted[i]
        if(i<lastCount){
            body += " "
        }
    }
    if(refStr == refStrings.type2){
        body = body.replace('with','')
    }
    if(refStr == refStrings.type4){
        body = body.replace('-','')
    }
    
    return body
}
const extractMentions = (strSplitted) => {
    let mentions = strSplitted.filter(x=>x.includes('@'))
    return mentions
}
export const matchString = (string) => {
    let strSplitted = string.split(' ')
    let firstWord = strSplitted[0].toLowerCase()
    let type 
    if(weekdays.findIndex(x=>x.toLowerCase() == firstWord) > 0){
        type = refStrings.type1
    }else if(checkForDateStr(firstWord)){
        type = refStrings.type2
    }else if(day.findIndex(x=>x.toLowerCase() == firstWord) > 0){
        type = refStrings.type4
    }else{
        type = refStrings.type3
    }
    let dateTime = extractDate(strSplitted,type)
    let mentions = extractMentions(strSplitted)
    for(var i=0;i<mentions.length;i++){
        mentions[i] = mentions[i].replace(',','')
    }
    let indexOfLastMention = strSplitted.findIndex(x=>x.replace(',','') == mentions[0])
    if(indexOfLastMention < 0){
        indexOfLastMention = strSplitted.length
    }
    let body = extractBody(strSplitted,type,indexOfLastMention)

    return {
        dateTime : dateTime,
        body : body,
        mentions : '['+mentions+']'
    }
}